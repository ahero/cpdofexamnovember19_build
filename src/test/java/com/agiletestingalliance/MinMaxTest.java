package com.agiletestingalliance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class MinMaxTest
{
	@Test
	public void MaxTest() throws Exception
	{
		MinMax minmax =  new MinMax();
		assertEquals(19, minmax.fMinMax(19, 7));
	}
	
	@Test
	public void MaxEqualTest() throws Exception
	{
		MinMax minmax =  new MinMax();
		assertEquals(88, minmax.fMinMax(88, 88));
	}

	@Test
	public void MaxNegativeTest() throws Exception
	{
		MinMax minmax =  new MinMax();
		assertNotEquals(3, minmax.fMinMax(3, 24));
	}
}
